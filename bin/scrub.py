#! /usr/bin/env python3
#
# Takes as stdin CSV, intended to be the nyt data snapshot
# Writes as output csv excluding rows that match any of the following patterns:
#   * `input` is empty
#   * `name` is empty
#   * `input` is the same as `unit`
#   * `input` is the same as `comment`
#
# Motivation: the nyt data appears to include some mistakes or degraded data
# this attempts to remove the worst offenders.

import csv
from sys import stdin, stdout



def isbad( row ):
    if row[1] == '':
        return True

    if row[2] == '':
        return True

    if row[1] == row[5]:
        return True

    if row[1] == row[6]:
        return True

    return False



class sane( csv.excel ):
    lineterminator = '\n'

reader = csv.reader( stdin )
writer = csv.writer( stdout, dialect = sane() )

writer.writerow( next( reader ) )
for row in reader:
    if not isbad( row ):
        writer.writerow( row )

