#! /usr/bin/env python3
#
# Takes as input some labelled data CSV
# Writes as output the input data CSV where some common units have been replaced
# with their common abbreviation in about half of the input records:
#   * "teaspoon" -> "tsp"
#   * "tablespoon" -> tbsp"
#   * "cup" -> "c"
#
# Motivation: the nyt data contains almost no abbreviations, so the trained model
# fails to label them as expected. These are the worst offenders as the unit is so
# commonly abbreviated in written recipes and the nyt data includes many orders of
# magnitude more complete word than abbreviation
#
# Here are the counts of each in the raw csv data (as counted by `grep -iw`):
# cup 40680
# cups 12453
# c 4
# teaspoon 24709
# teaspoons 6783
# tsp 16
# tablespoon 29782
# tablespoons 22124
# tbsp 7
# pound 11096
# pounds 5119
# lbs 4
# ounce 6986
# ounces 6061
# oz 164
# quart 536
# quarts 338
# qt 0
# gram 836
# g 12
#
# the goal is to make those proportions more balanced, such that there are
# a lot more samples to train the model to recognize the abbreviations


import csv
import random
import re
from sys import stdin, stdout

c_rx = re.compile( r'\bcups?\b' )
tsp_rx = re.compile( r'\bteaspoons?\b' )
tbsp_rx = re.compile( r'\btablespoons?\b' )
lbs_rx = re.compile( r'\bpounds?\b' )
oz_rx = re.compile( r'\bounces?\b' )
qt_rx = re.compile( r'\bquarts?\b' )
g_rx = re.compile( r'\bgrams?\b' )

def abbreviate( row ):
    if c_rx.search( row[1] ) is not None and c_rx.search( row[5] ) is not None:
        row[1] = c_rx.sub( 'c', row[1] )
        row[5] = c_rx.sub( 'c', row[5] )

    if tsp_rx.search( row[1] ) is not None and tsp_rx.search( row[5] ) is not None:
        row[1] = tsp_rx.sub( 'tsp', row[1] )
        row[5] = tsp_rx.sub( 'tsp', row[5] )

    if tbsp_rx.search( row[1] ) is not None and tbsp_rx.search( row[5] ) is not None:
        row[1] = tbsp_rx.sub( 'tbsp', row[1] )
        row[5] = tbsp_rx.sub( 'tbsp', row[5] )

    if lbs_rx.search( row[1] ) is not None and lbs_rx.search( row[5] ) is not None:
        row[1] = lbs_rx.sub( 'lbs', row[1] )
        row[5] = lbs_rx.sub( 'lbs', row[5] )

    if oz_rx.search( row[1] ) is not None and oz_rx.search( row[5] ) is not None:
        row[1] = oz_rx.sub( 'oz', row[1] )
        row[5] = oz_rx.sub( 'oz', row[5] )

    if qt_rx.search( row[1] ) is not None and qt_rx.search( row[5] ) is not None:
        row[1] = qt_rx.sub( 'qt', row[1] )
        row[5] = qt_rx.sub( 'qt', row[5] )

    if g_rx.search( row[1] ) is not None and g_rx.search( row[5] ) is not None:
        row[1] = g_rx.sub( 'g', row[1] )
        row[5] = g_rx.sub( 'g', row[5] )

    return row



class sane( csv.excel ):
    lineterminator = '\n'

reader = csv.reader( stdin )
writer = csv.writer( stdout, dialect = sane() )

writer.writerow( next( reader ) )
for row in reader:
    if random.choice( [ True, False ] ):
        row = abbreviate( row )
    writer.writerow( row )

